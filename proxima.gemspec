# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'proxima/version'

Gem::Specification.new do |spec|

  spec.name          = "proxima"
  spec.version       = Proxima::VERSION
  spec.authors       = ["Helmut Hissen"]
  spec.email         = ["helmut@zeebar.com"]
  spec.summary       = %q{Code for handling neighbourhood star system data.}
  spec.description   = %q{Proxima allows you to source and manipulate data describing close-by start systems.}
  spec.homepage      = "http://www.zeebar.com/projects/proxima"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "thor"

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "guard"
  spec.add_development_dependency "guard-rspec"
  spec.add_development_dependency "rake", "~> 10.0"

end

