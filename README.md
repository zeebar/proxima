# Proxima

This gem helps with sourcing stellar 3d data.  Initially, the code will support
David Nash's HYG Database (http://www.astronexus.com/hyg), but our intent is to
add support for other data sets over time depending on interest and availability.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'proxima'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install proxima

## Usage

    $ proxima catalog fetch 

Then, in your Ruby code: 

    require 'proxima'
     
    stars = Proxima.new
    stars.where(distance: 0..10).sort_by( :distance ).each do |star|
      puts "%-20s %4.2f %4.2f %4.2f" % [star.name, star.x, star.y, star.z]
    end

## Contributing

Small changes can be submitted via email, for anything else, please send us a pull request:

1. Fork it ( https://bitbucket.org.com/zeebar/proxima/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

Please test your changes using rspec.  We recommend using git flow (https://github.com/nvie/gitflow). 
    
