
require 'thor'
require 'proxima'

module Proxima

  class CatalogCLI < Thor

    desc "list", "list supported catalogs"

    def list
      Proxima::Catalog.each_available_catalog_type do |name, license_summary, summary|
        puts "%-12s %-20s %s" % [name, license_summary, summary]
      end
    end

    desc "use", "select current default catalog"

    def use( name )
    end

    desc "fetch", "fetch catalog"

    def fetch( name )
    end

    desc "license", "show licensing information"

    def license( name )
    end

  end

  class CLI < Thor

    desc "catalog SUBCOMMAND", "configure/fetch star catalog data"
    subcommand "catalog", CatalogCLI
 
    desc "raw <REF> [<SAMPLES> ...]", "compares audio files"

    option :format, type: :string, default: "csv"
    option :distance
    option :ra
    option :dec
    option :mag
    option :abs_mag
    option :spectrum

    def raw

      if files.empty?
        puts "no files specified"
      else
        ref_data = AudioSample.new(files[0])
        files[1..-1].each do |sample_file|
          sample_data = AudioSample.new(sample_file)
          match = sample_data.compare_with( ref_data )
          puts match.quality
        end
      end

    end

end

